# Contributing

Development follows the [GitLab
Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/).

## Code Style

Code should be formatted with [`black`](https://github.com/ambv/black) and
follow [PEP8](https://www.python.org/dev/peps/pep-0008/).

Be sure to have run the following commands before pushing commits. Otherwise,
the CI pipeline will only pass with warnings.

```bash
pip3 install --user black pycodestyle autopep8
# optionally run autopep8
autopep8 -arij0 .
# format all Python files in black style
black -l79 .
# check PEP8 compatibility
pycodestyle .
```

## Documentation

To build and view the documentation, run from the repository root

```bash
make docs
```

## Tests

To run the test suite, run from the repository root

```bash
python3 setup.py test
```

To get a test coverage, run

```bash
make coverage
```

## Releasing

This project uses [bumpversion](https://pypi.org/project/bumpversion/) for
versioning.

The usual steps to release a new version are the following:

1. update the version with `bumpversion patch` (or `minor` or `major`)
2. update the created tag's annotation via `git tag TAGNAME TAGNAME^{} -f -a`
3. publish the tag via `git push --tags`

The CI/CD pipelines handle the deployment.
