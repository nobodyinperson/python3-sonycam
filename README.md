# SonyCam - The Sony Camera Remote API for Python

[![pipeline status](https://gitlab.com/nobodyinperson/python3-sonycam/badges/master/pipeline.svg)](https://gitlab.com/nobodyinperson/python3-sonycam/commits/master) [![coverage report](https://gitlab.com/nobodyinperson/python3-sonycam/badges/master/coverage.svg)](https://nobodyinperson.gitlab.io/python3-sonycam/coverage-report/)
[![documentation](https://img.shields.io/badge/docs-sphinx-brightgreen.svg)](https://nobodyinperson.gitlab.io/python3-sonycam/) [![PyPI](https://badge.fury.io/py/sonycam.svg)](https://badge.fury.io/py/sonycam)

`sonycam` is a Python package to interface Sony cameras via the [Sony Camera
Remote API](https://developer.sony.com/develop/cameras).

## What can `sonycam` do?

- nothing currently...

## Installation

The `sonycam` package is best installed via `pip`. Run from anywhere:

```bash
python3 -m pip install --user sonycam
```

This downloads and installs the package from the [Python Package
Index](https://pypi.org).

You may also install `sonycam` via `pip3` from the repository root:

```bash
python3 -m pip install --user .
```

## Documentation

Documentation of the `sonycam` package can be found [here on
GitLab](https://nobodyinperson.gitlab.io/python3-sonycam).
