Welcome to sonycam's documentation!
===========================================

:mod:`sonycam` is a Python package to interface Sony cameras via the `Sony
Camera Remote API <https://developer.sony.com/develop/cameras>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   changelog
   api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
