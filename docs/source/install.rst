
Installation
============

:mod:`sonycam` is best installed via ``pip``::

    python3 -m pip install --user sonycam
